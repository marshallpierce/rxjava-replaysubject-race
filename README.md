Run `./gradlew build` and observe that the test that uses `subscribeOn()` will fail a few times out of thousands of runs.
